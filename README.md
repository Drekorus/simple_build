
## Windows Requirement
The OpenGL version is required to >= 4.5 on Windows.
Visual Studio 2017 is recommand if you want to debug into the code.
## Linux Requirement
The OpenGL version is required to >= 4.5 on Linux. To run vulkan of Dawn, please install vulkan driver on ubuntu.
If you are using Mesa driver, you should install the following library:
```sh
sudo apt-get install mesa-vulkan-drivers
```
If you are using Nvidia gpu, you should check if the driver support vulkan.
## macOS Requirement
The OpenGL version is required to >= 4.1 on macOS. To run Dawn/Metal backend, please check if your macOS support metal.
```sh
ps aux | grep -i "metal"
```

## Build OpenGL and Dawn backends

Mir uses gn to build on Linux, macOS and Windows.

```sh
# cd the repo
cd mir

# Download thirdparty
gclient sync

# Build on mir by ninja on Windows, Linux and macOS.
# On windows, opengl and dawn backends are enabled by default.
# On linux and macOS, opengl and dawn are enabled by default.
# Enable or disable a specific platform, you can add 'enable_opengl' and 'enable_dawn' to gn args.
# To build a release version, specify 'is_debug=false'.
gn gen out/Release --args="is_debug=false"
ninja -C out/Release mir

# Build on Windows by vs
gn gen out/build --ide=vs
open out/build/all.sln using visual studio.
build mir by vs

# Build on macOS by xcode
gn gen out/build --ide=xcode
build mir by xcode
```
